#%%
import hllhc_rematch_tools as lm_updated
import xtrack as xt
from cpymad.madx import Madx
import xpart as xp
import os
import xtrack._temp.lhc_match as lm
import matplotlib.pyplot as plt
import pandas as pd

def build_my_collider(save_to="collider_000_from_madx_thin_15cm.json"):
    #%% Create symbolic links to the optics folders

    # Creating the symbolic links
    result1 = lm_updated.create_symlink("/afs/cern.ch/eng/lhc/optics/HLLHCV1.5", "slhc")
    result2 = lm_updated.create_symlink("/afs/cern.ch/eng/lhc/optics/runIII", "lhc")

    print(result1, result2)

    myoptics = "/afs/cern.ch/eng/lhc/optics/HLLHCV1.6/round/opt_round_150_1500.madx"

    mad1=Madx()
    mad1.call('lhc/lhc.seq')
    mad1.call('slhc/hllhc_sequence.madx')
    mad1.input('beam, sequence=lhcb1, particle=proton, energy=7000;')
    mad1.use('lhcb1')
    mad1.call(f"{myoptics}")
    mad1.twiss()

    mad4=Madx()
    mad4.input('mylhcbeam=4')
    mad4.call('lhc/lhcb4.seq')
    mad4.call('slhc/hllhc_sequence.madx')
    mad4.input('beam, sequence=lhcb2, particle=proton, energy=7000;')
    mad4.use('lhcb2')
    mad4.call(f"{myoptics}")
    mad4.twiss()

    line1=xt.Line.from_madx_sequence(mad1.sequence.lhcb1,
                                    allow_thick=True,
                                    deferred_expressions=True,
                                    replace_in_expr={'bv_aux':'bvaux_b1'})
    line1.particle_ref = xp.Particles(mass0=xp.PROTON_MASS_EV, p0c=7000e9)

    line4=xt.Line.from_madx_sequence(mad4.sequence.lhcb2,
                                    allow_thick=True,
                                    deferred_expressions=True,
                                    replace_in_expr={'bv_aux':'bvaux_b2'})
    line4.particle_ref = xp.Particles(mass0=xp.PROTON_MASS_EV, p0c=7000e9)

    # Remove solenoids (cannot backtwiss for now)
    for ll in [line1, line4]:
        tt = ll.get_table()
        for nn in tt.rows[tt.element_type=='Solenoid'].name:
            ee_elen = ll[nn].length
            ll.element_dict[nn] = xt.Drift(length=ee_elen)

    line1.twiss_default['method']='4d'
    line4.twiss_default['method']='4d'
    line4.twiss_default['reverse']=True

    line1.build_tracker()
    line4.build_tracker()

    
    collider = xt.Multiline(lines={'lhcb1':line1,'lhcb2':line4})
    # if folder colliders does not exist create it
    if not os.path.exists('colliders'):
        os.makedirs('colliders')
    collider.to_json(f"colliders/{save_to}")
    return collider


def build_my_collider_16(save_to="collider_000_from_madx_thin_15cm.json"):
    #%% Create symbolic links to the optics folders

    # Creating the symbolic links
    import os
    import subprocess

    # Folder to check and clone into if it does not exist
    folder_name = "hllhc16"

    # Check if the folder exists
    if not os.path.exists(folder_name):
        # Folder does not exist, so clone the git repository
        print(f"Folder '{folder_name}' not found. Cloning the repository.")
        subprocess.run([
            "git", "clone", "-b", "hl16", 
            "https://gitlab.cern.ch/acc-models/acc-models-lhc.git", 
            folder_name
        ])
    else:
        # Folder exists
        print(f"Folder '{folder_name}' already exists.")

    myoptics = "hllhc16/strengths/round/opt_round_150_1500.madx"

    mad1=Madx()
    mad1.call('hllhc16/lhc.seq')
    mad1.call('hllhc16/hllhc_sequence.madx')
    mad1.input('beam, sequence=lhcb1, particle=proton, energy=7000;')
    mad1.use('lhcb1')
    mad1.call(f"{myoptics}")
    mad1.twiss()

    mad4=Madx()
    mad4.input('mylhcbeam=4')
    mad4.call('hllhc16/lhcb4.seq')
    mad4.call('hllhc16/hllhc_sequence.madx')
    mad4.input('beam, sequence=lhcb2, particle=proton, energy=7000;')
    mad4.use('lhcb2')
    mad4.call(f"{myoptics}")
    mad4.twiss()

    line1=xt.Line.from_madx_sequence(mad1.sequence.lhcb1,
                                    allow_thick=True,
                                    deferred_expressions=True,
                                    replace_in_expr={'bv_aux':'bvaux_b1'})
    line1.particle_ref = xp.Particles(mass0=xp.PROTON_MASS_EV, p0c=7000e9)

    line4=xt.Line.from_madx_sequence(mad4.sequence.lhcb2,
                                    allow_thick=True,
                                    deferred_expressions=True,
                                    replace_in_expr={'bv_aux':'bvaux_b2'})
    line4.particle_ref = xp.Particles(mass0=xp.PROTON_MASS_EV, p0c=7000e9)

    # Remove solenoids (cannot backtwiss for now)
    for ll in [line1, line4]:
        tt = ll.get_table()
        for nn in tt.rows[tt.element_type=='Solenoid'].name:
            ee_elen = ll[nn].length
            ll.element_dict[nn] = xt.Drift(length=ee_elen)

    line1.twiss_default['method']='4d'
    line4.twiss_default['method']='4d'
    line4.twiss_default['reverse']=True

    line1.build_tracker()
    line4.build_tracker()

    
    collider = xt.Multiline(lines={'lhcb1':line1,'lhcb2':line4})
    # if folder colliders does not exist create it
    if not os.path.exists('colliders'):
        os.makedirs('colliders')
    collider.to_json(f"colliders/{save_to}")
    return collider



def check_after_loading_collider(collider):
    tw = collider.twiss()

    print(tw["lhcb1"][["betx", "bety", "px", "py", "x", "y"], "ip.*"])
    print(tw["lhcb2"][["betx", "bety", "px", "py", "x", "y"], "ip.*"])
    import matplotlib.pyplot as plt

    fig, axs = plt.subplots(figsize=(21, 11))
    axs.plot(tw["lhcb1"].s, tw["lhcb1"].betx, label=r"$\beta_x$", color="black")
    axs.plot(tw["lhcb1"].s, tw["lhcb1"].bety, label=r"$\beta_y$", color="red")
    axs_t = axs.twiny()
    axs_t.set_xticks(
        tw["lhcb1"][["s"], "ip.*"],
        tw["lhcb1"][["name"], "ip.*"],
        rotation="horizontal",
    )
    axs_t.set_xlim(axs.get_xlim()[0], axs.get_xlim()[1])
    axs.set_ylabel(r"$\beta_{x,y} [m]$", fontsize=30)
    axs.set_xlabel(r"$s [m]$", fontsize=30)
    axs.legend(fontsize=30)
    axs.grid()
    plt.show()  

def check_after_strength_limits(collider):
    print(collider.vars.vary_default)

def check_before_arcs_rematch(collider):
    arc_periodic_solution = lm.get_arc_periodic_solution(collider)
    for beam in ["b1","b2"]:
        print(f"Beam {beam[-1]}")
        for arc in ["12", "23", "34", "45", "56", "67", "78", "81"]:
            print(f"Arc{arc} H after matching:", "value=", round(arc_periodic_solution[f"lhc{beam}"][arc]["mux"][-1],4), ",target= ", round(collider.varval[f"mux{arc}{beam}"], 4), ",Diff=", round(arc_periodic_solution[f"lhc{beam}"][arc]["mux"][-1]-collider.varval[f"mux{arc}{beam}"], 8))  

def get_info_from_optimizers(opt):
    print("Vary:")
    print(opt._vary_table())
    print("")
    print("Targets:")
    print(opt._targets_table())
    print("")
    print("Knob values:")
    print(opt.get_knob_values())

def check_after_arcs_rematch(collider, **kwargs):
    arc_periodic_solution = lm.get_arc_periodic_solution(collider)
    for beam in ["b1","b2"]:
        print(f"Beam {beam[-1]}")
        for arc in ["12", "23", "34", "45", "56", "67", "78", "81"]:
            print(f"Arc{arc} H after matching:", "value=", round(arc_periodic_solution[f"lhc{beam}"][arc]["mux"][-1],4), ",target= ", round(collider.varval[f"mux{arc}{beam}"], 4), ",Diff=", round(arc_periodic_solution[f"lhc{beam}"][arc]["mux"][-1]-collider.varval[f"mux{arc}{beam}"], 8))  
    if not kwargs:
        return
    elif kwargs["optimizers"]:
        optimizers = kwargs["optimizers"]
        for arc in optimizers.keys():
            print("#"*50)
            print(f"Optimizer for arc {arc}:")
            get_info_from_optimizers(optimizers[arc])

def check_before_ir15_rematch(collider, **kwargs):
    arc_periodic_solution = lm.get_arc_periodic_solution(collider)
    for bn in ['b1', 'b2']:
        for arc in ["81", "45"]:
            twiss_before_rematch = collider[f"lhc{bn}"].twiss(start=f's.ds.l{arc[-1]}.{bn}', end=f'e.ds.r{arc[-1]}.{bn}', init=arc_periodic_solution[f"lhc{bn}"][arc].get_twiss_init(f's.ds.l{arc[-1]}.{bn}') )
            print(f"Values before rematching {bn} IP{arc[-1]}:")   
            print(twiss_before_rematch[["betx", "bety"], "ip.*"])  


def check_after_ir15_rematch(collider, **kwargs):
    arc_periodic_solution = lm.get_arc_periodic_solution(collider)
    for bn in ['b1', 'b2']:
        for arc in ["81", "45"]:
            twiss_after_rematch = collider[f"lhc{bn}"].twiss(start=f's.ds.l{arc[-1]}.{bn}', end=f'e.ds.r{arc[-1]}.{bn}', init=arc_periodic_solution[f"lhc{bn}"][arc].get_twiss_init(f's.ds.l{arc[-1]}.{bn}') )
            print(f"Values after rematching {bn} IP{arc[-1]}:")   
            print(twiss_after_rematch[["betx", "bety", "alfx", "alfy", "dx"], "ip.*"])
    if not kwargs:
        return
    elif kwargs["optimizers"]:
        optimizers = kwargs["optimizers"]
        get_info_from_optimizers(optimizers)

def check_before_ir3_rematch(collider, **kwargs):
    tw = collider.twiss()
    #print(collider.varval[f'betxip3b1'])
    print(tw["lhcb1"][["betx", "bety"], "ip3"])
    print(tw["lhcb2"][["betx", "bety"], "ip3"])

def check_after_ir3_rematch(collider, **kwargs):
    tw = collider.twiss()
    #print(collider.varval[f'betxip3b1'])
    print(tw["lhcb1"][["betx", "bety"], "ip3"])
    print(tw["lhcb2"][["betx", "bety"], "ip3"])
    if not kwargs:
        return
    elif kwargs["optimizers"]:
        optimizers = kwargs["optimizers"]
        get_info_from_optimizers(optimizers)

def check_before_ir7_rematch(collider, **kwargs):
    tw = collider.twiss()
    #print(collider.varval[f'betxip3b1'])
    print(tw["lhcb1"][["betx", "bety"], "ip7"])
    print(tw["lhcb2"][["betx", "bety"], "ip7"])

def check_after_ir7_rematch(collider, **kwargs):
    tw = collider.twiss()
    #print(collider.varval[f'betxip3b1'])
    print(tw["lhcb1"][["betx", "bety"], "ip7"])
    print(tw["lhcb2"][["betx", "bety"], "ip7"])
    if not kwargs:
        return
    elif kwargs["optimizers"]:
        optimizers = kwargs["optimizers"]
        get_info_from_optimizers(optimizers)

def check_ats_targets_and_twiss(collider, **kwargs):
    if not kwargs:
        return
    else:
        arc_periodic_solution = kwargs["arc_periodic_solution"]
        tw_sq_a81_ip1_a12 = kwargs["tw_sq_a81_ip1_a12"]
        all_arcs_periodic_solution = pd.concat([arc_periodic_solution["lhcb1"]["12"].to_pandas(), 
                                                arc_periodic_solution["lhcb1"]["81"].to_pandas()
                                                ])

        merged_df = pd.merge(tw_sq_a81_ip1_a12["b1"].to_pandas(), all_arcs_periodic_solution, 
                            on="name")
        merged_df.sort_values(by = "s_x", inplace=True)


        fig, ax = plt.subplots(nrows=2, sharex=True, figsize=(14,10))
        plt.sca(ax[0])
        plt.plot(merged_df["s_x"], merged_df["betx_x"], c="k",label="ATS arcs Squeeze")
        plt.plot(merged_df["s_x"], merged_df["betx_y"], c="b", label="ATS arcs Pre-squeeze")
        print("Ratio of maximum arc betax function: ", merged_df["betx_x"].max()/merged_df["betx_y"].max())
        plt.xlabel("s [m]")
        plt.ylabel(r"$\beta_x$ [m]")
        plt.legend()
        plt.sca(ax[1])
        plt.plot(merged_df["s_x"], merged_df["bety_x"], c="k",label="ATS arcs Squeeze")
        plt.plot(merged_df["s_x"], merged_df["bety_y"], c="b", label="ATS arcs Pre-squeeze")
        print("Ratio of maximum arc betay function: ", merged_df["bety_x"].max()/merged_df["bety_y"].max())
        plt.xlabel("s [m]")
        plt.ylabel(r"$\beta_y$ [m]")
        plt.legend()


def check_before_ir2_rematch(collider, **kwargs):
    tw = collider.twiss()
    #print(collider.varval[f'betxip3b1'])
    print(tw["lhcb1"][["betx", "bety"], "ip2"])
    print(tw["lhcb2"][["betx", "bety"], "ip2"])

def check_after_ir2_rematch(collider, **kwargs):
    tw = collider.twiss()
    #print(collider.varval[f'betxip3b1'])
    print(tw["lhcb1"][["betx", "bety"], "ip2"])
    print(tw["lhcb2"][["betx", "bety"], "ip2"])
    if not kwargs:
        return
    elif kwargs["optimizers"]:
        optimizers = kwargs["optimizers"]
        get_info_from_optimizers(optimizers)

def check_before_ir8_rematch(collider):
    tw = collider.twiss()
    #print(collider.varval[f'betxip3b1'])
    print(tw["lhcb1"][["betx", "bety"], "ip8"])
    print(tw["lhcb2"][["betx", "bety"], "ip8"])

def check_after_ir8_rematch(collider, **kwargs):
    tw = collider.twiss()
    #print(collider.varval[f'betxip3b1'])
    print(tw["lhcb1"][["betx", "bety"], "ip8"])
    print(tw["lhcb2"][["betx", "bety"], "ip8"])
    if not kwargs:
        return
    elif kwargs["optimizers"]:
        optimizers = kwargs["optimizers"]
        get_info_from_optimizers(optimizers)

def check_before_ir4_rematch(collider, **kwargs):
    tw = collider.twiss()
    #print(collider.varval[f'betxip3b1'])
    print(tw["lhcb1"][["betx", "bety"], "ip4"])
    print(tw["lhcb2"][["betx", "bety"], "ip4"])

def check_after_ir4_rematch(collider, **kwargs):
    tw = collider.twiss()
    #print(collider.varval[f'betxip3b1'])
    print(tw["lhcb1"][["betx", "bety"], "ip4"])
    print(tw["lhcb2"][["betx", "bety"], "ip4"])
    if not kwargs:
        return
    elif kwargs["optimizers"]:
        optimizers = kwargs["optimizers"]
        get_info_from_optimizers(optimizers)

def check_before_ir6_rematch(collider, **kwargs):
    tw = collider.twiss()
    #print(collider.varval[f'betxip3b1'])
    print(tw["lhcb1"][["betx", "bety"], "ip6"])
    print(tw["lhcb2"][["betx", "bety"], "ip6"])

def check_after_ir6_rematch(collider, **kwargs):
    tw = collider.twiss()
    #print(collider.varval[f'betxip3b1'])
    print(tw["lhcb1"][["betx", "bety"], "ip6"])
    print(tw["lhcb2"][["betx", "bety"], "ip6"])
    if not kwargs:
        return
    elif kwargs["optimizers"]:
        optimizers = kwargs["optimizers"]
        get_info_from_optimizers(optimizers)

def check_sext(collider):
    for bn in ["b1", "b2"]:
        for flag in ["d", "f"]:
            for sext_flag in ["1","2"]:
                for arc in ["12", "23", "34", "45", "56", "67", "78", "81"]:     
                    print(f'ks{flag}{sext_flag}.a{arc}{bn}', collider.varval[f'ks{flag}{sext_flag}.a{arc}{bn}'])

def check_orbit_knobs(collider):

    myknobs = ["on_x2v", "on_x2h", "on_x8v", "on_x8h",
                "on_sep2h", "on_sep2v", "on_sep8h", "on_sep8v",
                "on_o8h", "on_o8v", "on_o2h", "on_o2v",
                "on_a8h", "on_a8v", "on_a2h", "on_a2v"]

    for ip in [2,8]:
        for var, myknob, factor in zip(["py", "px", "py", "px",
                                "y", "x", "y", "x",
                                "y", "x", "y", "x",], 
                            [f"on_x{ip}v", f"on_x{ip}h", 
                                f"on_a{ip}v", f"on_a{ip}h",
                                f"on_o{ip}v", f"on_o{ip}h",
                                f"on_sep{ip}v", f"on_sep{ip}h"],
                                [1e6, 1e6, 1e6, 1e6, 1e3, 1e3, 1e3, 1e3]):
            target = 5
            collider.vars[myknob] = target
            tw = collider.twiss()
            collider.vars[myknob] = 0
            print(myknob, "Target: ", target, "Value B1/B2: ", round(tw.lhcb1[var, f"ip{ip}"]*factor,4), round(tw.lhcb2[var, f"ip{ip}"]*factor,4))

    default_value_ondsip = collider.vars["on_disp"]._value
    collider.vars["on_disp"] = 0

    target = 250
    collider.vars["on_x1"] = target
    tw = collider.twiss()
    collider.vars["on_x1"] = 0
    print("on_x1 Target: ", target, "Value B1/B2: ", round(tw.lhcb1["px", f"ip1"]*1e6,4), round(tw.lhcb2["px", f"ip1"]*1e6,4))

    collider.vars["on_x5"] = target
    tw = collider.twiss()
    collider.vars["on_x5"] = 0
    print("on_x5 Target: ", target, "Value B1/B2: ", round(tw.lhcb1["py", f"ip5"]*1e6,4), round(tw.lhcb2["py", f"ip5"]*1e6,4))

    for ip in [1,5]:
            for var, myknob, factor in zip(["y", "x", "y", "x", "py", "px"], 
                                    [f"on_sep{ip}v", f"on_sep{ip}h",
                                    f"on_o{ip}v", f"on_o{ip}h",
                                    f"on_a{ip}v", f"on_a{ip}h",],

                                    [ 1e3, 1e3, 1e3, 1e3, 1e6, 1e6]):
                target = 0.8
                collider.vars[myknob] = target
                tw = collider.twiss()
                collider.vars[myknob] = 0
                print(myknob, "Target: ", target, "Value B1/B2: ", round(tw.lhcb1[var, f"ip{ip}"]*factor,4), round(tw.lhcb2[var, f"ip{ip}"]*factor,4))

    collider.vars["on_disp"] = default_value_ondsip
# %%
def check_chroma(collider):
    tw = collider.twiss()
    print("Chromaticity H/V B1/B2: ", tw.lhcb1.dqx, tw.lhcb1.dqy,tw.lhcb2.dqx, tw.lhcb2.dqy)

def check_W(collider):
    tw = collider.twiss()
    fig, ax = plt.subplots(nrows=2, sharex=True, figsize=(14,10))
    plt.sca(ax[0])
    plt.grid()
    plt.plot(tw.lhcb1.s, tw.lhcb1.wx_chrom, label="H")
    plt.plot(tw.lhcb1.s, tw.lhcb1.wy_chrom, label="V")
    #plt.xlabel("s [m]")
    plt.title("B1")
    plt.ylabel("Wx/y")
    plt.legend()
    plt.sca(ax[1])
    plt.grid()
    plt.plot(tw.lhcb2.s, tw.lhcb2.wx_chrom, label="H")
    plt.plot(tw.lhcb2.s, tw.lhcb2.wy_chrom, label="V")
    plt.xlabel("s [m]")
    plt.ylabel("Wx/y")
    plt.title("B2")
    print(tw.lhcb1[["ax_chrom", "bx_chrom", "wx_chrom"], ["ip3", "ip7", "ip1", "ip5"]])
    print(tw.lhcb2[["ax_chrom", "bx_chrom", "wx_chrom"], ["ip3", "ip7", "ip1", "ip5"]])
    return tw

def check_dispersion(collider):
    tw= collider.twiss()
    print(tw.lhcb1[["dx", "dy"], "ip.*"])