# %%
import hllhc_rematch_tools
from hllhc_rematch_tools import *
import os
import matplotlib.pyplot as plt

get_ipython().run_line_magic("matplotlib", "inline")
get_ipython().run_line_magic("load_ext", "autoreload")
get_ipython().run_line_magic("autoreload", "2")


# %%
def set_vars(collider, config):
    print(config)
    for key, val in config.items():
        collider.varval[key] = val


configs = {
    "on_0": {
        "cd2q4": 0,
        "on_x1": 0,
        "on_x5": 0,
        "on_sep1": 0,
        "on_sep5": 0,
        "phi_ir1": 0,
        "phi_ir5": 90,
        "on_a1": 0,
        "on_a5": 0,
    },
    "on_x1hs": {
        "cd2q4": 0,
        "on_x1": 250,
        "on_x5": 0,
        "on_sep1": 0,
        "phi_ir1": 0,
        "phi_ir5": 90,
    },
    "on_x1hl": {
        "cd2q4": 1,
        "on_x1": 250,
        "on_x5": 0,
        "on_sep1": 0,
        "phi_ir1": 0,
        "phi_ir5": 90,
    },
    "on_x1vs": {
        "cd2q4": 0,
        "on_x1": 250,
        "on_x5": 0,
        "on_sep1": 0,
        "phi_ir1": 90,
        "phi_ir5": 0,
    },
    "on_x1vl": {
        "cd2q4": 1,
        "on_x1": 250,
        "on_x5": 0,
        "on_sep1": 0,
        "phi_ir1": 90,
        "phi_ir5": 0,
    },
    "on_x5hs": {
        "cd2q4": 0,
        "on_x1": 0,
        "on_x5": 250,
        "on_sep1": 0,
        "phi_ir1": 90,
        "phi_ir5": 0,
    },
    "on_x5hl": {
        "cd2q4": 1,
        "on_x1": 0,
        "on_x5": 250,
        "on_sep1": 0,
        "phi_ir1": 90,
        "phi_ir5": 0,
    },
    "on_x5vs": {
        "cd2q4": 0,
        "on_x1": 0,
        "on_x5": 250,
        "on_sep1": 0,
        "phi_ir1": 0,
        "phi_ir5": 90,
    },
    "on_x5vl": {
        "cd2q4": 1,
        "on_x1": 0,
        "on_x5": 250,
        "on_sep1": 0,
        "phi_ir1": 0,
        "phi_ir5": 90,
    },
    "on_x15hvs": {
        "cd2q4": 0,
        "on_x1": 250,
        "on_x5": 250,
        "on_sep1": 0,
        "phi_ir1": 0,
        "phi_ir5": 90,
    },
    "on_x15hvl": {
        "cd2q4": 1,
        "on_x1": 250,
        "on_x5": 250,
        "on_sep1": 0,
        "phi_ir1": 0,
        "phi_ir5": 90,
    },
    "on_sep1v": {"on_sep1": 2, "phi_ir1": 0},
    "on_sep5h": {"on_sep5": 2, "phi_ir5": 90},
    "on_sep1h": {"on_sep1": 2, "phi_ir1": 90},
    "on_sep5v": {"on_sep5": 2, "phi_ir5": 0},
    "on_a1v": {"on_a1": 1},
    "on_a1h": {"on_a1": 1, "phi_ir1": 90},
    "on_a5h": {"on_a5": 1},
    "on_a5v": {"on_a5": 1, "phi_ir5": 0},
}


def plot_orbits(collider1, collider2, line_name):
    tw1 = collider1[line_name].twiss(only_markers=True)
    tw2 = collider2[line_name].twiss(only_markers=True)
    fig, axs = plt.subplots(2, 1, figsize=(21, 22))
    axs[0].set_title(f"{line_name}")
    axs[0].plot(tw1.s, tw1.x, label="x, old", color="black")
    axs[0].plot(tw1.s, tw1.y, label="y, old", color="red")
    axs[0].plot(tw2.s, tw2.x, label="x, new", ls="--", color="black")
    axs[0].plot(tw2.s, tw2.y, label="y, new", ls="--", color="red")

    axs[1].plot(tw1.s, tw1.x - tw2.x, label="x, old-new", color="black")
    axs[1].plot(tw1.s, tw1.y - tw2.y, label="y, old-new", color="red")
    for i in range(2):
        axs_t = axs[i].twiny()
        axs_t.set_xticks(
            tw1[["s"], "ip.*"], tw1[["name"], "ip.*"], rotation="horizontal"
        )
        axs_t.set_xlim(axs[i].get_xlim()[0], axs[i].get_xlim()[1])
        axs[i].set_ylabel(r"$co [m]$")
        axs[i].set_xlabel(r"$s [m]$")
        axs[i].legend()
        axs[i].grid()
    plt.show()


# %%
hllhc_path = "../../hllhc16"
# optics_name = "opt/opt_collapse_1100_1500.madx"
optics_name = "../opt/opt_round_150_1500.madx"
# optics_name = "../summer_optics/SoL/opt_levelling_580_1500.madx"

# %%
collider = None
collider_name = "collider_hl16.json"
if os.path.exists(collider_name):
    collider = xt.Multiline.from_json(collider_name)
else:
    collider = build_collider(hllhc_path, optics_name, cycling=False)
    collider.to_json(collider_name)

# %%
collider.vars.load_madx_optics_file(optics_name)

# %%
lm.set_var_limits_and_steps(collider)

set_var_limits_and_steps_orbit_ip15(collider)  # correctors for orbit in 1,5
set_var_limits_and_steps_quads_ip15(collider)  # quads in 1,5
set_var_limits_and_steps_sextupoles_w(collider)  # for chromatic functions
set_var_limits_and_steps_disp_correctors(collider)  # dispersion orbit correctors
# %%
collider["lhcb1"].cycle("s.ds.l3.b1", inplace=True)
collider["lhcb2"].cycle("s.ds.l3.b2", inplace=True)

collider.lhcb1.twiss_default["method"] = "4d"
collider.lhcb2.twiss_default["method"] = "4d"
collider.lhcb2.twiss_default["reverse"] = True
# %%
tw_init = collider["lhcb1"].twiss()
tw2_init = collider["lhcb2"].twiss()

# %%
collider.vars["on_disp"] = 1
set_vars(collider, configs["on_0"])
set_vars(collider, configs["on_x1hs"])
# set_vars(collider, configs["on_sep1v"])
plot_orbit(collider, "lhcb1")
collider.vars["on_disp"] = 0
set_vars(collider, configs["on_0"])

# %%
c0 = collider.copy()

# %%
default_tol = {None: 1e-8, "betx": 1e-6, "bety": 1e-6}

opt = rematch_disp(
    collider, solve=True, default_tol=default_tol, angle_ref=295, sep_ref=1, cycle=False
)

# %%
for opp in opt.values():
    print(opp.log())
# %%
c0.vars["on_disp"] = 1
collider.vars["on_disp"] = 1
# %%
set_vars(c0, configs["on_0"])
set_vars(c0, configs["on_x1hs"])
set_vars(c0, configs["on_sep1v"])
set_vars(collider, configs["on_0"])
set_vars(collider, configs["on_x1hs"])
set_vars(collider, configs["on_sep1v"])
# plot_orbit(c0, "lhcb1")
# plot_orbit(collider, "lhcb1")
# plot_orbit(c0, "lhcb2")
# plot_orbit(collider, "lhcb2")

plot_orbits(c0, collider, "lhcb1")
plot_orbits(c0, collider, "lhcb2")
# %%
set_vars(c0, configs["on_0"])
set_vars(c0, configs["on_x1vs"])
set_vars(c0, configs["on_sep1h"])
set_vars(collider, configs["on_0"])
set_vars(collider, configs["on_x1vs"])
set_vars(collider, configs["on_sep1h"])

# plot_orbit(c0, "lhcb1")
# plot_orbit(collider, "lhcb1")
# plot_orbit(c0, "lhcb2")
# plot_orbit(collider, "lhcb2")

plot_orbits(c0, collider, "lhcb1")
plot_orbits(c0, collider, "lhcb2")

c0.vars["phi_ir1"] = 0
collider.vars["phi_ir1"] = 0
# %%
set_vars(c0, configs["on_0"])
set_vars(c0, configs["on_x1hl"])
set_vars(c0, configs["on_sep1v"])
set_vars(collider, configs["on_0"])
set_vars(collider, configs["on_x1hl"])
set_vars(collider, configs["on_sep1v"])

# plot_orbit(c0, "lhcb1")
# plot_orbit(collider, "lhcb1")
# plot_orbit(c0, "lhcb2")
# plot_orbit(collider, "lhcb2")

plot_orbits(c0, collider, "lhcb1")
plot_orbits(c0, collider, "lhcb2")

# %%
set_vars(c0, configs["on_0"])
set_vars(c0, configs["on_x5vs"])
set_vars(c0, configs["on_sep5h"])
set_vars(collider, configs["on_0"])
set_vars(collider, configs["on_x5vs"])
set_vars(collider, configs["on_sep5h"])

plot_orbits(c0, collider, "lhcb1")
plot_orbits(c0, collider, "lhcb2")
# %%
set_vars(c0, configs["on_0"])
set_vars(c0, configs["on_x5vl"])
set_vars(c0, configs["on_sep5h"])
set_vars(collider, configs["on_0"])
set_vars(collider, configs["on_x5vl"])
set_vars(collider, configs["on_sep5h"])

plot_orbits(c0, collider, "lhcb1")
plot_orbits(c0, collider, "lhcb2")
# %%

set_vars(c0, configs["on_0"])
set_vars(c0, configs["on_x5hs"])
set_vars(c0, configs["on_sep5v"])
set_vars(collider, configs["on_0"])
set_vars(collider, configs["on_x5hs"])
set_vars(collider, configs["on_sep5v"])

plot_orbits(c0, collider, "lhcb1")
plot_orbits(c0, collider, "lhcb2")
# %%

set_vars(c0, configs["on_0"])
set_vars(c0, configs["on_x5hl"])
set_vars(c0, configs["on_sep5v"])
set_vars(collider, configs["on_0"])
set_vars(collider, configs["on_x5hl"])
set_vars(collider, configs["on_sep5v"])

plot_orbits(c0, collider, "lhcb1")
plot_orbits(c0, collider, "lhcb2")

# %%
