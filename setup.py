from setuptools import setup, find_packages

setup(
    name="hllhc_rematch_tools",
    version="0.0.1",
    description="Rematch HL-LHC Optics Tools",
    author="Yannis Angelis",
    author_email="ioannis.angelis@cern.ch",
    url="",
    packages=find_packages(),
    install_requires=[
        "numpy",
        "matplotlib",
        "cpymad",
        "xsuite",
    ],
)
