import xtrack as xt


def corchroma_weak(collider, qprime, default_tol=None, nsteps=10):
    """
    correct Q' using the SF/SD families of sectors 23/34/56/67
    """

    ksf_names = (
        "ksf1.a12bim ksf2.a12bim ksf1.a81bim ksf2.a81bim "
        "ksf1.a56bim ksf2.a56bim ksf1.a45bim ksf2.a45bim"
    )

    ksd_names = (
        "ksd1.a12bim ksd2.a12bim ksd1.a81bim ksd2.a81bim "
        "ksd1.a56bim ksd2.a56bim ksd1.a45bim ksd2.a45bim"
    )

    old_vals = {}

    for ksfd in ksf_names.split() + ksd_names.split():
        for bim in ["b1", "b2"]:
            ks_name = ksfd.replace("bim", bim)
            old_vals[ks_name] = collider.varval[ks_name]
            collider.vars[ks_name] = old_vals[ks_name]

    ksf_names_23346778 = (
        "ksf1.a23bim ksf2.a23bim ksf1.a34bim ksf2.a34bim "
        "ksf1.a67bim ksf2.a67bim ksf1.a78bim ksf2.a78bim"
    )

    ksd_names_23346778 = (
        "ksd1.a23bim ksd2.a23bim ksd1.a34bim ksd2.a34bim "
        "ksd1.a67bim ksd2.a67bim ksd1.a78bim ksd2.a78bim"
    )

    for ksfd in ksf_names_23346778.split():
        for bim in ["b1", "b2"]:
            ksf_name = ksfd.replace("bim", bim)
            old_vals[ksf_name] = collider.varval[ksf_name]
            collider.vars[ksf_name] = old_vals[ksf_name] + collider.vars[f"ksf.{bim}"]

    for ksfd in ksd_names_23346778.split():
        for bim in ["b1", "b2"]:
            ksd_name = ksfd.replace("bim", bim)
            old_vals[ksd_name] = collider.varval[ksd_name]
            collider.vars[ksd_name] = old_vals[ksd_name] + collider.vars[f"ksd.{bim}"]

    opt_b1 = collider["lhcb1"].match(
        vary=[xt.VaryList(["ksf.b1", "ksd.b1"])],
        targets=[xt.TargetSet(dqx=qprime, dqy=qprime, tol=1e-6)],
        solve=False,
        assert_within_tol=False,
        restore_if_fail=False,
        solver_options={"min_step": 1e-7, "n_steps_max": nsteps},
        default_tol=default_tol,
    )
    opt_b1.solve()

    opt_b2 = collider["lhcb2"].match(
        vary=[xt.VaryList(["ksf.b2", "ksd.b2"])],
        targets=[xt.TargetSet(dqx=qprime, dqy=qprime, tol=1e-6)],
        solve=False,
        assert_within_tol=False,
        restore_if_fail=False,
        solver_options={"min_step": 1e-7, "n_steps_max": nsteps},
        default_tol=default_tol,
    )
    opt_b2.solve()

    for ksfd in ksf_names_23346778.split():
        for bim in ["b1", "b2"]:
            ksf_name = ksfd.replace("bim", bim)
            collider.vars[ksf_name] = old_vals[ksf_name] + collider.varval[f"ksf.{bim}"]

    for ksfd in ksd_names_23346778.split():
        for bim in ["b1", "b2"]:
            ksd_name = ksfd.replace("bim", bim)
            collider.vars[ksd_name] = old_vals[ksd_name] + collider.varval[f"ksd.{bim}"]

    collider.vars["ksf.b1"] = 0
    collider.vars["ksd.b1"] = 0
    collider.vars["ksf.b2"] = 0
    collider.vars["ksd.b2"] = 0

    return opt_b1, opt_b2


def rematch_w(collider, line_name, default_tol=None, solve=False, nsteps=4):
    sign_I = 1
    bim = line_name[-2:]
    line = collider["lhcb1"].cycle("ip3")
    if line_name == "lhcb2":
        sign_I = -1
    tw = line.twiss(method="4d", only_markers=True)

    start_range = "mbxf.4l1_entry"
    end_range = "ip1"
    twiss_init = tw.get_twiss_init(start_range)
    twiss_init.ax_chrom = 0
    twiss_init.bx_chrom = 0
    twiss_init.ay_chrom = 0
    twiss_init.by_chrom = 0
    tw_l1 = line.twiss(
        start=start_range,
        end=end_range,
        init=twiss_init,
        compute_chromatic_properties=True,
    )

    start_range = "mbxf.4l5_entry"
    end_range = "ip5"
    twiss_init = tw.get_twiss_init(start_range)
    twiss_init.ax_chrom = 0
    twiss_init.bx_chrom = 0
    twiss_init.ay_chrom = 0
    twiss_init.by_chrom = 0
    tw_l5 = line.twiss(
        start=start_range,
        end=end_range,
        init=twiss_init,
        compute_chromatic_properties=True,
    )

    # Measure chromaticities of IR5
    start_range = "ip1"
    end_range = "mbxf.4r1_exit"
    twiss_init = tw.get_twiss_init(end_range)
    twiss_init.ax_chrom = 0
    twiss_init.bx_chrom = 0
    twiss_init.ay_chrom = 0
    twiss_init.by_chrom = 0
    tw_r1 = line.twiss(
        start=start_range,
        end=end_range,
        init=twiss_init,
        compute_chromatic_properties=True,
    )

    start_range = "ip5"
    end_range = "mbxf.4r5_exit"
    twiss_init = tw.get_twiss_init(end_range)
    twiss_init.ax_chrom = 0
    twiss_init.bx_chrom = 0
    twiss_init.ay_chrom = 0
    twiss_init.by_chrom = 0
    tw_r5 = line.twiss(
        start=start_range,
        end=end_range,
        init=twiss_init,
        compute_chromatic_properties=True,
    )

    tar_ix1 = -0.5 * (tw_l1.dmux[-1] - tw_r1.dmux[-1])
    tar_iy1 = 0.5 * (tw_l1.dmuy[-1] - tw_r1.dmuy[-1])
    tar_ix5 = -0.5 * (tw_l5.dmux[-1] - tw_r5.dmux[-1])
    tar_iy5 = 0.5 * (tw_l5.dmuy[-1] - tw_r5.dmuy[-1])

    print(f"{tar_ix1=}")
    print(f"{tar_iy1=}")
    print(f"{tar_ix5=}")
    print(f"{tar_iy5=}")

    vary_list_b1 = xt.VaryList(
        [
            "ksf1.a81b1",
            "ksd2.a81b1",
            "ksf1.a12b1",
            "ksd2.a12b1",
            "ksf1.a45b1",
            "ksd2.a45b1",
            "ksf1.a56b1",
            "ksd2.a56b1",
        ]
    )

    vary_list_b2 = xt.VaryList(
        [
            "ksf2.a81b2",
            "ksd1.a81b2",
            "ksf2.a12b2",
            "ksd1.a12b2",
            "ksf2.a45b2",
            "ksd1.a45b2",
            "ksf2.a56b2",
            "ksd1.a56b2",
        ]
    )

    vary_list = vary_list_b1 if line_name == "lhcb1" else vary_list_b2

    opt = collider[line_name].match(
        solve=False,
        assert_within_tol=False,
        restore_if_fail=False,
        verbose=False,
        solver_options={"n_bisections": 1, "min_step": 1e-4, "n_steps_max": nsteps},
        default_tol=default_tol,
        n_steps_max=nsteps,
        vary=vary_list,
        targets=[
            # xt.Target(
            #     lambda tt: tt["wx_chrom", "ip3"] - tt["wx_chrom", "ip7"], value=0
            # ),
            # xt.Target(
            #     lambda tt: tt["wy_chrom", "ip3"] - tt["wy_chrom", "ip7"], value=0
            # ),
            # IR3, IR7
            xt.Target(at="ip3", tar="bx_chrom", value=0),
            xt.Target(at="ip3", tar="by_chrom", value=0),
            xt.Target(at="ip3", tar="ax_chrom", value=0),
            xt.Target(at="ip3", tar="ay_chrom", value=0),
            xt.Target(at="ip7", tar="bx_chrom", value=0),
            xt.Target(at="ip7", tar="by_chrom", value=0),
            xt.Target(at="ip7", tar="ax_chrom", value=0),
            xt.Target(at="ip7", tar="ay_chrom", value=0),
            # IP1, IP5
            xt.Target(at="ip1", tar="bx_chrom", value=0),
            xt.Target(at="ip1", tar="by_chrom", value=0),
            xt.Target(at="ip1", tar="ax_chrom", value=tar_ix1 * sign_I),
            xt.Target(at="ip1", tar="ay_chrom", value=-tar_iy1 * sign_I),
            xt.Target(at="ip5", tar="bx_chrom", value=0),
            xt.Target(at="ip5", tar="by_chrom", value=0),
            xt.Target(at="ip5", tar="ax_chrom", value=tar_ix5 * sign_I),
            xt.Target(at="ip5", tar="ay_chrom", value=-tar_iy5 * sign_I),
        ],
    )

    opt.disable_vary(tag="w2")

    if solve:
        opt.solve()
    return opt
