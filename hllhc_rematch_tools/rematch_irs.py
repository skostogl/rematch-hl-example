import xtrack as xt
import xtrack._temp.lhc_match as lm



def connect_lr_qx(collider, nqx=0, ir=5):
    """Connect specified right kqx in IP5 of IP1 with the left kqx"""
    assert ir == 1 or ir == 5
    collider.vars[f"kqx{nqx}.r{ir}"] = -collider.vars[f"kqx{nqx}.l{ir}"]


def connect_lr_qs(collider, nq=0, ir=5, bim=1):
    """Connect specified kq in ir for either beam with the left kq"""
    collider.vars[f"kq{nq}.r{ir}b{bim}"] = -collider.vars[f"kq{nq}.l{ir}b{bim}"]


def connect_q2s(collider, ir=5):
    """Connect Q2 in IP5, or IP1 to be used as one for matching"""
    assert ir == 1 or ir == 5
    collider.vars[f"kqx2b.l{ir}"] = +collider.vars[f"kqx2a.l{ir}"]
    collider.vars[f"kqx2a.r{ir}"] = -collider.vars[f"kqx2a.l{ir}"]
    collider.vars[f"kqx2b.r{ir}"] = -collider.vars[f"kqx2a.l{ir}"]


def rematch_ir15(
    collider,
    betxip5,
    betyip5,
    tw_sq_a45_ip5_a56=None,
    match_on_triplet=0,
    match_inj_tunes=0,
    no_match_beta=False,
    ir5q4sym=0,
    ir5q5sym=0,
    ir5q6sym=0,
    restore=True,
    solve=False,
    ratio_at_CC=1.0,
    beta_peak_ratio=0.995,
    n_steps=25,
    imb=1.5,
    default_tol=None,
    apq789=0,
    apq5=0,
    apq6=0,
    apq4=0,
    apq1011=0,
    match_on_aperture=0,
    epsap=0.0,
):
    """Match IP5 for the beta_x, beta_y, and copy strenghts to IP1

    Parameters
    ----------
    collider : xtrack.Multiline
        HL-LHC collider with b1 and b2
    betxip5 : float
        betax for ip1 and ip5
    betyip5 : float
        betay for ip1 and ip5
    tw_sq_a45_ip5_a56 : dict
        Dictionary for beam1, beam2 with twiss tables for arcs 45, 56
    match_on_triplet: int
        Flag to select how to match the triplet.
        0 : Don't touch the triplet
        1 : Q1, Q2, Q3 free
        2 : Q2 free
        3 :
        4 : Link Q1 with Q3
    match_inj_tunes: bool
    no_match_beta: bool
        If False, match beta at the ip
    ir5q4sym: int
        Flag to connect Q4
        0 : Connect Q4 left for both beams and Q4 right for both beams
        1 : Connect Q4 left and right
    ir5q5sym: int
        Flag to connect Q5
        0 : Connect Q5 left for both beams and Q5 right for both beams
        1 : Connect Q5 left and right
    ir5q6sym: int
        Flag to connect Q6
        0 : Connect Q6 left for both beams and Q6 right for both beams
        1 : Connect Q6 left and right
    restore: bool
        Flag to restore the variables if the match fails
    solve: bool
        Flag to solve or not

    """

    if tw_sq_a45_ip5_a56 is None:
        tw_sq_a45_ip5_a56 = lm.get_arc_periodic_solution(
            collider, arc_name=["45", "56"]
        )

    targets = []

    if no_match_beta == False:
        targets.append(
            xt.TargetSet(
                line="lhcb1",
                at="ip5",
                betx=betxip5,
                bety=betyip5,
                alfx=0,
                alfy=0,
                dx=0,
                dpx=0,
            )
        )
        targets.append(
            xt.TargetSet(
                line="lhcb2",
                at="ip5",
                betx=betxip5,
                bety=betyip5,
                alfx=0,
                alfy=0,
                dx=0,
                dpx=0,
            )
        )
    else:
        targets.append(
            xt.TargetSet(line="lhcb1", at="ip5", alfx=0, alfy=0, dx=0, dpx=0)
        )
        targets.append(
            xt.TargetSet(line="lhcb2", at="ip5", alfx=0, alfy=0, dx=0, dpx=0)
        )

    targets.append(
        xt.TargetSet(
            line="lhcb1",
            at="e.ds.r5.b1",
            betx=tw_sq_a45_ip5_a56["lhcb1"]["56"]["betx", "e.ds.r5.b1"],
            bety=tw_sq_a45_ip5_a56["lhcb1"]["56"]["bety", "e.ds.r5.b1"],
            alfx=tw_sq_a45_ip5_a56["lhcb1"]["56"]["alfx", "e.ds.r5.b1"],
            alfy=tw_sq_a45_ip5_a56["lhcb1"]["56"]["alfy", "e.ds.r5.b1"],
            dx=tw_sq_a45_ip5_a56["lhcb1"]["56"]["dx", "e.ds.r5.b1"],
            dpx=tw_sq_a45_ip5_a56["lhcb1"]["56"]["dpx", "e.ds.r5.b1"],
        )
    )
    targets.append(
        xt.TargetSet(
            line="lhcb2",
            at="e.ds.r5.b2",
            betx=tw_sq_a45_ip5_a56["lhcb2"]["56"]["betx", "e.ds.r5.b2"],
            bety=tw_sq_a45_ip5_a56["lhcb2"]["56"]["bety", "e.ds.r5.b2"],
            alfx=tw_sq_a45_ip5_a56["lhcb2"]["56"]["alfx", "e.ds.r5.b2"],
            alfy=tw_sq_a45_ip5_a56["lhcb2"]["56"]["alfy", "e.ds.r5.b2"],
            dx=tw_sq_a45_ip5_a56["lhcb2"]["56"]["dx", "e.ds.r5.b2"],
            dpx=tw_sq_a45_ip5_a56["lhcb2"]["56"]["dpx", "e.ds.r5.b2"],
        )
    )

    muxIP5b1_l = collider["lhcb1"].varval["muxip5b1_l"]
    muxIP5b1_r = collider["lhcb1"].varval["muxip5b1_r"]
    muyIP5b1_l = collider["lhcb1"].varval["muyip5b1_l"]
    muyIP5b1_r = collider["lhcb1"].varval["muyip5b1_r"]
    muxIP5b1 = muxIP5b1_l + muxIP5b1_r
    muyIP5b1 = muyIP5b1_l + muyIP5b1_r

    muxIP5b2_l = collider["lhcb2"].varval["muxip5b2_l"]
    muxIP5b2_r = collider["lhcb2"].varval["muxip5b2_r"]
    muyIP5b2_l = collider["lhcb2"].varval["muyip5b2_l"]
    muyIP5b2_r = collider["lhcb2"].varval["muyip5b2_r"]
    muxIP5b2 = muxIP5b2_l + muxIP5b2_r
    muyIP5b2 = muyIP5b2_l + muyIP5b2_r

    if match_inj_tunes == 0:
        targets.append(
            xt.TargetSet(line="lhcb1", at="ip5", mux=muxIP5b1_l, muy=muyIP5b1_l)
        )
        targets.append(
            xt.TargetSet(line="lhcb1", at="e.ds.r5.b1", mux=muxIP5b1, muy=muyIP5b1)
        )
        targets.append(
            xt.TargetSet(line="lhcb2", at="ip5", mux=muxIP5b2_l, muy=muyIP5b2_l)
        )
        targets.append(
            xt.TargetSet(line="lhcb2", at="e.ds.r5.b2", mux=muxIP5b2, muy=muyIP5b2)
        )

    else:
        targets.append(
            xt.TargetSet(
                line="lhcb1",
                at="e.ds.r5.b1",
                mux=muxIP5b1,
                muy=muyIP5b1,
            )
        )
        targets.append(
            xt.TargetSet(
                line="lhcb2",
                at="e.ds.r5.b2",
                mux=muxIP5b2,
                muy=muyIP5b2,
            )
        )
    if (apq789>100):
        targets.append(
            xt.TargetSet(
                'bety',xt.LessThan(apq789),line="lhcb1",
                at="mqm.a7l5.b1",
            )
        )
        targets.append(
            xt.TargetSet(
                'betx',xt.LessThan(apq789),line="lhcb1",
                at="mqml.8l5.b1",
            )
        )
        targets.append(
            xt.TargetSet(
                'bety',xt.LessThan(apq789),line="lhcb1",
                at="mqm.9l5.b1",
            )
        )
        targets.append(
            xt.TargetSet(
                'betx',xt.LessThan(apq789),line="lhcb1",
                at="mqm.a7r5.b1",
            )
        )
        targets.append(
            xt.TargetSet(
                'bety',xt.LessThan(apq789),line="lhcb1",
                at="mqml.8r5.b1",
            )
        )
        targets.append(
            xt.TargetSet(
                'betx',xt.LessThan(apq789),line="lhcb1",
                at="mqm.9r5.b1",
            )
        )
        targets.append(
            xt.TargetSet(
                'betx',xt.LessThan(apq789),line="lhcb2",
                at="mqm.a7l5.b2",
            )
        )
        targets.append(
            xt.TargetSet(
                'bety',xt.LessThan(apq789),line="lhcb2",
                at="mqml.8l5.b2",
            )
        )
        targets.append(
            xt.TargetSet(
                'betx',xt.LessThan(apq789),line="lhcb2",
                at="mqm.9l5.b2",
            )
        )
        targets.append(
            xt.TargetSet(
                'bety',xt.LessThan(apq789),line="lhcb2",
                at="mqm.a7r5.b2",
            )
        )
        targets.append(
            xt.TargetSet(
                'betx',xt.LessThan(apq789),line="lhcb2",
                at="mqml.8r5.b2",
            )
        )
        targets.append(
            xt.TargetSet(
                'bety',xt.LessThan(apq789),line="lhcb2",
                at="mqm.9r5.b2",
            )
        )
    if (apq5>100):
        targets.append(
            xt.TargetSet(
                'bety',xt.LessThan(apq5),line="lhcb1",
                at="mqml.5l5.b1",
            )
        )
        targets.append(
            xt.TargetSet(
                'betx',xt.LessThan(apq5),line="lhcb1",
                at="mqml.5r5.b1",
            )
        )
        targets.append(
            xt.TargetSet(
                'betx',xt.LessThan(apq5),line="lhcb2",
                at="mqml.5l5.b2",
            )
        )
        targets.append(
            xt.TargetSet(
                'bety',xt.LessThan(apq5),line="lhcb2",
                at="mqml.5r5.b2",
            )
        )
    if (apq6>100):
        targets.append(
            xt.TargetSet(
                'betx',xt.LessThan(apq6),line="lhcb1",
                at="mqml.6l5.b1",
            )
        )
        targets.append(
            xt.TargetSet(
                'bety',xt.LessThan(apq6),line="lhcb1",
                at="mqml.6r5.b1",
            )
        )
        targets.append(
            xt.TargetSet(
                'bety',xt.LessThan(apq6),line="lhcb2",
                at="mqml.6l5.b2",
            )
        )
        targets.append(
            xt.TargetSet(
                'betx',xt.LessThan(apq6),line="lhcb2",
                at="mqml.6r5.b2",
            )
        )
    if (apq4>100):
        targets.append(
            xt.TargetSet(
                'betx',xt.LessThan(apq4),line="lhcb1",
                at="mqy.4l5.b1",
            )
        )
        targets.append(
            xt.TargetSet(
                'bety',xt.LessThan(apq4),line="lhcb1",
                at="mqy.4r5.b1",
            )
        )
        targets.append(
            xt.TargetSet(
                'bety',xt.LessThan(apq4),line="lhcb2",
                at="mqy.4l5.b2",
            )
        )
        targets.append(
            xt.TargetSet(
                'betx',xt.LessThan(apq4),line="lhcb2",
                at="mqy.4r5.b2",
            )
        )
    if (apq1011>100):
        targets.append(
            xt.TargetSet(
                'bety',xt.LessThan(apq1011),line="lhcb1",
                at="mq.11l5.b1",
            )
        )
        targets.append(
            xt.TargetSet(
                'betx',xt.LessThan(apq1011),line="lhcb1",
                at="mq.11r5.b1",
            )
        )
        targets.append(
            xt.TargetSet(
                'bety',xt.LessThan(apq1011),line="lhcb2",
                at="mq.11r5.b2",
            )
        )
        targets.append(
            xt.TargetSet(
                'betx',xt.LessThan(apq1011),line="lhcb2",
                at="mq.11l5.b2",
            )
        )
        targets.append(
            xt.TargetSet(
                'betx',xt.LessThan(apq1011),line="lhcb1",
                at="mqml.10l5.b1",
            )
        )
        targets.append(
            xt.TargetSet(
                'bety',xt.LessThan(apq1011),line="lhcb1",
                at="mqml.10r5.b1",
            )
        )
        targets.append(
            xt.TargetSet(
                'bety',xt.LessThan(apq1011),line="lhcb2",
                at="mqml.10l5.b2",
            )
        )
        targets.append(
            xt.TargetSet(
                'betx',xt.LessThan(apq1011),line="lhcb2",
                at="mqml.10r5.b2",
            )
        )
    if match_on_aperture == 1:
        targets.append(
            xt.TargetSet(
                'bety',xt.LessThan(173-epsap),line="lhcb2",
                at="mqml.10l5.b2",
            )
        )
        targets.append(
            xt.TargetSet(
                'bety',xt.LessThan(178-epsap),line="lhcb1",
                at="mq.11l5.b1",
            )
        )
        targets.append(
            xt.TargetSet(
                'bety',xt.LessThan(183.2-epsap),line="lhcb2",
                at="mq.11r5.b2",
            )
        )
        targets.append(
            xt.TargetSet(
                'betx',xt.LessThan(167-epsap),line="lhcb1",
                at="mqml.8r5.b1",
            )
        )
        targets.append(
            xt.TargetSet(
                'bety',xt.LessThan(167-epsap),line="lhcb2",
                at="mqml.8l5.b2",
            )
        )



    vary_list = []

    if ir5q4sym == 0:
        collider.vars["imq4l"] = (
            -collider.vars["kq4.l5b2"] / collider.vars["kq4.l5b1"] / 100
        )
        collider.vars["imq4r"] = (
            -collider.vars["kq4.r5b2"] / collider.vars["kq4.r5b1"] / 100
        )

        collider.vars["kq4.l5b2"] = -(
            collider.vars["kq4.l5b1"] * collider.vars["imq4l"] * 100
        )
        collider.vars["kq4.r5b2"] = -(
            collider.vars["kq4.r5b1"] * collider.vars["imq4r"] * 100
        )

        if (
            collider.varval["imq4l"] < 1 / imb / 100
            or collider.varval["imq4l"] > imb / 100
        ):
            collider.vars["imq4l"]._set_value(1 / imb / 100)
        if (
            collider.varval["imq4r"] < 1 / imb / 100
            or collider.varval["imq4r"] > imb / 100
        ):
            collider.vars["imq4r"]._set_value(1 / imb / 100)

        vary_list.append(xt.Vary("imq4l", limits=(1 / imb / 100, imb / 100), step=1e-6))
        vary_list.append(xt.Vary("imq4r", limits=(1 / imb / 100, imb / 100), step=1e-6))
        vary_list.append(xt.Vary("kq4.l5b1"))
        vary_list.append(xt.Vary("kq4.r5b1"))
    elif ir5q4sym == 1:
        connect_lr_qs(collider, nq=4, ir=5, bim=1)
        connect_lr_qs(collider, nq=4, ir=5, bim=2)
        vary_list.append(xt.Vary("kq4.l5b1"))
        vary_list.append(xt.Vary("kq4.l5b2"))

    if ir5q5sym == 0:
        collider.vars["imq5l"] = (
            -collider.vars["kq5.l5b2"] / collider.vars["kq5.l5b1"] / 100
        )
        collider.vars["imq5r"] = (
            -collider.vars["kq5.r5b2"] / collider.vars["kq5.r5b1"] / 100
        )

        collider.vars["kq5.l5b2"] = -(
            collider.vars["kq5.l5b1"] * collider.vars["imq5l"] * 100
        )
        collider.vars["kq5.r5b2"] = -(
            collider.vars["kq5.r5b1"] * collider.vars["imq5r"] * 100
        )

        if (
            collider.varval["imq5l"] < 1 / imb / 100
            or collider.varval["imq5l"] > imb / 100
        ):
            collider.vars["imq5l"]._set_value(1 / imb / 100)
        if (
            collider.varval["imq5r"] < 1 / imb / 100
            or collider.varval["imq5r"] > imb / 100
        ):
            collider.vars["imq5r"]._set_value(1 / imb / 100)

        vary_list.append(xt.Vary("imq5l", limits=(1 / imb / 100, imb / 100), step=1e-6))
        vary_list.append(xt.Vary("imq5r", limits=(1 / imb / 100, imb / 100), step=1e-6))
        vary_list.append(xt.Vary("kq5.l5b1"))
        vary_list.append(xt.Vary("kq5.r5b1"))
    elif ir5q5sym == 1:
        connect_lr_qs(collider, nq=5, ir=5, bim=1)
        connect_lr_qs(collider, nq=5, ir=5, bim=2)
        vary_list.append(xt.Vary("kq5.l5b1"))
        vary_list.append(xt.Vary("kq5.l5b2"))

    if ir5q6sym == 0:
        collider.vars["imq6l"] = (
            -collider.vars["kq6.l5b2"] / collider.vars["kq6.l5b1"] / 100
        )
        collider.vars["imq6r"] = (
            -collider.vars["kq6.r5b2"] / collider.vars["kq6.r5b1"] / 100
        )

        collider.vars["kq6.l5b2"] = -(
            collider.vars["kq6.l5b1"] * collider.vars["imq6l"] * 100
        )
        collider.vars["kq6.r5b2"] = -(
            collider.vars["kq6.r5b1"] * collider.vars["imq6r"] * 100
        )

        if (
            collider.varval["imq6l"] < 1 / imb / 100
            or collider.varval["imq6l"] > imb / 100
        ):
            collider.vars["imq6l"]._set_value(1 / imb / 100)
        if (
            collider.varval["imq6r"] < 1 / imb / 100
            or collider.varval["imq6r"] > imb / 100
        ):
            collider.vars["imq6r"]._set_value(1 / imb / 100)

        vary_list.append(xt.Vary("imq6l", limits=(1 / imb / 100, imb / 100), step=1e-6))
        vary_list.append(xt.Vary("imq6r", limits=(1 / imb / 100, imb / 100), step=1e-6))
        vary_list.append(xt.Vary("kq6.l5b1"))
        vary_list.append(xt.Vary("kq6.r5b1"))
    elif ir5q6sym == 1:
        connect_lr_qs(collider, nq=6, ir=5, bim=1)
        connect_lr_qs(collider, nq=6, ir=5, bim=2)
        vary_list.append(xt.Vary("kq6.l5b1"))
        vary_list.append(xt.Vary("kq6.l5b2"))

    quads_ir5_b1 = (
        "kq4.l5b1 kq4.r5b1 kq5.l5b1 kq5.r5b1 kq6.l5b1 kq6.r5b1 "
        "kq7.l5b1 kq7.r5b1 kq8.l5b1 kq8.r5b1 kq9.l5b1 kq9.r5b1 "
        "kq10.l5b1 kq10.r5b1 kqtl11.l5b1 kqtl11.r5b1 "
        "kqt12.l5b1 kqt12.r5b1 kqt13.l5b1 kqt13.r5b1"
    )

    quads_ir5_b2 = (
        "kq4.l5b2 kq4.r5b2 kq5.l5b2 kq5.r5b2 kq6.l5b2 kq6.r5b2 "
        "kq7.l5b2 kq7.r5b2 kq8.l5b2 kq8.r5b2 kq9.l5b2 kq9.r5b2 "
        "kq10.l5b2 kq10.r5b2 kqtl11.l5b2 kqtl11.r5b2 kqt12.l5b2 "
        "kqt12.r5b2 kqt13.l5b2 kqt13.r5b2"
    )
    # Beam 1
    for quad in quads_ir5_b1.split()[6:]:
        vary_list.append(xt.Vary(quad))

    # Beam 2
    for quad in quads_ir5_b2.split()[6:]:
        vary_list.append(xt.Vary(quad))

    if match_on_triplet == 0:  # q1 q2 q3 untouched
        pass
    elif match_on_triplet == 1:  # Q1 Q2 Q3 free
        connect_q2s(collider)
        connect_lr_qx(collider, 1)
        connect_lr_qx(collider, 3)
        vary_list.append(xt.Vary("kqx1.l5"))
        vary_list.append(xt.Vary("kqx2a.l5"))
        vary_list.append(xt.Vary("kqx3.l5"))
    elif match_on_triplet == 2:
        # connect kqx2a and kqx2b left and right
        connect_q2s(collider)
        vary_list.append(xt.Vary("kqx2a.l5"))
    elif match_on_triplet == 4:
        # link q3 and q1

        connect_lr_qx(collider, 1)
        collider.vars["kqx3.l5"] = +collider.vars["kqx1.l5"]
        collider.vars["kqx3.r5"] = -collider.vars["kqx1.l5"]

        connect_q2s(collider)
        vary_list.append(xt.Vary("kqx1.l5"))
        vary_list.append(xt.Vary("kqx2a.l5"))
    elif match_on_triplet == 5:
        connect_q2s(collider)
        connect_lr_qx(collider, 1)
        connect_lr_qx(collider, 3)
        vary_list.append(xt.Vary("kqx1.l5"))
        vary_list.append(xt.Vary("kqx2a.l5"))
        targets.append(
            xt.Target(
                lambda tt: tt.lhcb1["bety", "mqxfb.a2r5_exit"]
                / tt.lhcb1["betx", "mqxfa.b3r5_exit"],
                value=beta_peak_ratio,
                tol=1e-2,
            )
        )

    twiss_init_b1 = tw_sq_a45_ip5_a56["lhcb1"]["45"].get_twiss_init("s.ds.l5.b1")
    twiss_init_b1.mux = 0
    twiss_init_b1.muy = 0

    twiss_init_b2 = tw_sq_a45_ip5_a56["lhcb2"]["45"].get_twiss_init("s.ds.l5.b2")
    twiss_init_b2.mux = 0
    twiss_init_b2.muy = 0

    opt = collider.match(
        solve=False,
        only_markers=False,
        method="4d",
        solver_options={"n_bisections": 3, "min_step": 1e-7, "n_steps_max": n_steps},
        default_tol=default_tol,
        start=["s.ds.l5.b1", "s.ds.l5.b2"],
        end=["e.ds.r5.b1", "e.ds.r5.b2"],
        restore_if_fail=restore,
        assert_within_tol=False,
        init=[twiss_init_b1, twiss_init_b2],
        targets=targets,
        vary=vary_list,
        verbose=False,
    )

    if solve:
        opt.solve()

    collider.vars["kqx1.r5"] = -collider.varval["kqx1.l5"]
    collider.vars["kqx2.l5"] = collider.varval["kqx2a.l5"]
    collider.vars["kqx2a.l5"] = collider.varval["kqx2.l5"]
    collider.vars["kqx2b.l5"] = collider.varval["kqx2.l5"]
    collider.vars["kqx2a.r5"] = -collider.varval["kqx2.l5"]
    collider.vars["kqx2b.r5"] = -collider.varval["kqx2.l5"]
    collider.vars["kqx3.r5"] = -collider.varval["kqx3.l5"]

    collider.vars["kqx1.l1"] = collider.varval["kqx1.l5"]
    collider.vars["kqx2a.l1"] = collider.varval["kqx2a.l5"]
    collider.vars["kqx2b.l1"] = collider.varval["kqx2b.l5"]
    collider.vars["kqx3.l1"] = collider.varval["kqx3.l5"]
    collider.vars["kqx1.r1"] = collider.varval["kqx1.r5"]
    collider.vars["kqx2a.r1"] = collider.varval["kqx2a.r5"]
    collider.vars["kqx2b.r1"] = collider.varval["kqx2b.r5"]
    collider.vars["kqx3.r1"] = collider.varval["kqx3.r5"]

    # remove the imqXlr knobs if any
    quads_imqlr = "kq4.l5b2 kq4.r5b2 kq5.l5b2 kq5.r5b2 kq6.l5b2 kq6.r5b2"
    for quad in quads_imqlr.split():
        collider.vars[quad] = collider.varval[quad]

    for quad in quads_ir5_b1.split():
        collider.vars[quad.replace("5b", "1b")] = collider.varval[quad]

    for quad in quads_ir5_b2.split():
        collider.vars[quad.replace("5b", "1b")] = collider.varval[quad]

    return opt


def rematch_ir2(collider, line_name,
                boundary_conditions_left, boundary_conditions_right,
                mux_ir2, muy_ir2, betx_ip2, bety_ip2,
                solve=True, staged_match=False, default_tol=None):

    assert line_name in ['lhcb1', 'lhcb2']
    bn = line_name[-2:]

    opt = collider[f'lhc{bn}'].match(
        solve=False,
        default_tol=default_tol,
        start=f's.ds.l2.{bn}', end=f'e.ds.r2.{bn}',
        # Left boundary
        init=boundary_conditions_left, init_at=xt.START,
        targets=[
            xt.TargetSet(at=xt.END,
                    tars=('betx', 'bety', 'alfx', 'alfy', 'dx', 'dpx'),
                    value=boundary_conditions_right, tag='stage0'),
            xt.TargetSet(at='ip2',
                betx=betx_ip2, bety=bety_ip2, alfx=0, alfy=0, dx=0, dpx=0,
                tag='stage2'),
            xt.TargetRelPhaseAdvance('mux', mux_ir2, tag='stage0'),
            xt.TargetRelPhaseAdvance('muy', muy_ir2, tag='stage0'),
        ],
        vary=[
            xt.VaryList([
                f'kq9.l2{bn}', f'kq10.l2{bn}', f'kqtl11.l2{bn}', f'kqt12.l2{bn}', f'kqt13.l2{bn}',
                f'kq9.r2{bn}', f'kq10.r2{bn}', f'kqtl11.r2{bn}', f'kqt12.r2{bn}', f'kqt13.r2{bn}'],
                tag='stage0'),
            xt.VaryList(
                [f'kq4.l2{bn}', f'kq5.l2{bn}',  f'kq6.l2{bn}',    f'kq7.l2{bn}',   f'kq8.l2{bn}',
                 f'kq6.r2{bn}',    f'kq7.r2{bn}',   f'kq8.r2{bn}'],
                tag='stage1'),
            xt.VaryList([f'kq4.r2{bn}', f'kq5.r2{bn}'], tag='stage2')
        ]
    )

    if solve:
        if staged_match:
            opt.disable_all_vary()
            opt.disable_all_targets()

            opt.enable_vary(tag='stage0')
            opt.enable_targets(tag='stage0')
            opt.solve()

            opt.enable_vary(tag='stage1')
            opt.solve()

            opt.enable_vary(tag='stage2')
            opt.enable_targets(tag='stage2')
            opt.solve()
        else:
            opt.solve()

    return opt

def rematch_ir3(collider, line_name,
                boundary_conditions_left, boundary_conditions_right,
                mux_ir3, muy_ir3,
                alfx_ip3, alfy_ip3,
                betx_ip3, bety_ip3,
                dx_ip3, dpx_ip3,
                match_on_aperture=0, epsap=0.0,
                solve=True, staged_match=False, default_tol=None):

    assert line_name in ['lhcb1', 'lhcb2']
    bn = line_name[-2:]

    targets=[
            xt.TargetSet(at='ip3',
                    alfx=alfx_ip3, alfy=alfy_ip3, betx=betx_ip3, bety=bety_ip3,
                    dx=dx_ip3, dpx=dpx_ip3, tag='stage1'),
            xt.TargetSet(at=xt.END,
                    tars=('betx', 'bety', 'alfx', 'alfy', 'dx', 'dpx'),
                    value=boundary_conditions_right),
            xt.TargetRelPhaseAdvance('mux', mux_ir3),
            xt.TargetRelPhaseAdvance('muy', muy_ir3),
        ]
    
    if match_on_aperture == 1:
        if bn == 'b1':
            lhcb1_targets = [
                    xt.TargetSet('bety', xt.LessThan(184.4 + epsap),  at="mq.11l3.b1"),
                    xt.TargetSet('bety', xt.LessThan(178.7 + epsap), at="mq.9l3.b1"),
                    xt.TargetSet('bety', xt.LessThan(185.5 + epsap),  at="mq.8r3.b1"),
                    xt.TargetSet('bety', xt.LessThan(170 + epsap),  at="mq.10r3.b1"),
                    xt.TargetSet('betx', xt.LessThan(184.95 + epsap),  at="mq.11r3.b1"),
            ]
            targets.extend(lhcb1_targets)
        else:
            lhcb2_targets = [
                    xt.TargetSet('betx', xt.LessThan(184.5 - 3.5), at="mq.11l3.b2"),
                    xt.TargetSet('bety', xt.LessThan(167.5),  at="mq.10l3.b2"),
                    xt.TargetSet('bety', xt.LessThan(180 + 2), at="mq.8l3.b2"),
                    xt.TargetSet('bety', xt.LessThan(182 + 0.2), at="mq.9r3.b2"),
                    xt.TargetSet('bety', xt.LessThan(193.4 + 0.5), at="mq.11r3.b2"),
                    xt.TargetSet('betx', xt.LessThan(179.6 + 0.2),  at="mq.12r3.b2"),
                    xt.TargetSet('dx', xt.LessThan(2.15 + 0.01),  at="mq.12r3.b2")

            ]
            targets.extend(lhcb2_targets)
    print(targets)

    opt = collider[f'lhc{bn}'].match(
        solve=False,
        default_tol=default_tol,
        start=f's.ds.l3.{bn}', end=f'e.ds.r3.{bn}',
        init=boundary_conditions_left, init_at=xt.START,
        targets=targets,
        vary=[
            xt.VaryList([f'kqt13.l3{bn}', f'kqt12.l3{bn}', f'kqtl11.l3{bn}',
                         f'kqtl10.l3{bn}', f'kqtl9.l3{bn}', f'kqtl8.l3{bn}',
                         f'kqtl7.l3{bn}', f'kq6.l3{bn}',
                         f'kq6.r3{bn}', f'kqtl7.r3{bn}',
                         f'kqtl8.r3{bn}', f'kqtl9.r3{bn}', f'kqtl10.r3{bn}',
                         f'kqtl11.r3{bn}', f'kqt12.r3{bn}', f'kqt13.r3{bn}'])
        ]
    )

    if solve:
        if staged_match:
            opt.disable_targets(tag='stage1')
            opt.solve()
            opt.enable_targets(tag='stage1')
            opt.solve()
        else:
            opt.solve()

    return opt

def rematch_ir4(collider, line_name,
                boundary_conditions_left, boundary_conditions_right,
                mux_ir4, muy_ir4,
                alfx_ip4, alfy_ip4,
                betx_ip4, bety_ip4,
                dx_ip4, dpx_ip4,
                solve=True, staged_match=False, default_tol=None):

    assert line_name in ['lhcb1', 'lhcb2']
    bn = line_name[-2:]

    opt = collider[f'lhc{bn}'].match(
        solve=False,
        default_tol=default_tol,
        start=f's.ds.l4.{bn}', end=f'e.ds.r4.{bn}',
        init=boundary_conditions_left, init_at=xt.START,
        targets=[
            xt.TargetSet(at='ip4',
                    alfx=alfx_ip4, alfy=alfy_ip4, betx=betx_ip4, bety=bety_ip4,
                    dx=dx_ip4, dpx=dpx_ip4, tag='stage1'),
            xt.TargetSet(at=xt.END,
                    tars=('betx', 'bety', 'alfx', 'alfy', 'dx', 'dpx'),
                    value=boundary_conditions_right),
            xt.TargetRelPhaseAdvance('mux', mux_ir4),
            xt.TargetRelPhaseAdvance('muy', muy_ir4),
        ],
        vary=[
            xt.VaryList([
                f'kqt13.l4{bn}', f'kqt12.l4{bn}', f'kqtl11.l4{bn}', f'kq10.l4{bn}',
                f'kq9.l4{bn}', f'kq8.l4{bn}', f'kq7.l4{bn}', f'kq6.l4{bn}',
                f'kq5.l4{bn}', f'kq5.r4{bn}', f'kq6.r4{bn}', f'kq7.r4{bn}',
                f'kq8.r4{bn}', f'kq9.r4{bn}', f'kq10.r4{bn}', f'kqtl11.r4{bn}',
                f'kqt12.r4{bn}', f'kqt13.r4{bn}'
            ])
        ]
    )

    if solve:
        if staged_match:
            opt.disable_targets(tag='stage1')
            opt.solve()
            opt.enable_targets(tag='stage1')
            opt.solve()
        else:
            opt.solve()

    return opt


def rematch_ir6(collider, line_name,
                boundary_conditions_left, boundary_conditions_right,
                mux_ir6, muy_ir6,
                alfx_ip6, alfy_ip6,
                betx_ip6, bety_ip6,
                dx_ip6, dpx_ip6,
                solve=True, staged_match=False, default_tol=None):

    assert line_name in ['lhcb1', 'lhcb2']
    bn = line_name[-2:]

    opt = collider[f'lhc{bn}'].match(
        solve=False,
        default_tol=default_tol,
        start=f's.ds.l6.{bn}', end=f'e.ds.r6.{bn}',
        # Left boundary
        init=boundary_conditions_left, init_at=xt.START,
        targets=[
            xt.TargetSet(at='ip6',
                    alfx=alfx_ip6, alfy=alfy_ip6, betx=betx_ip6, bety=bety_ip6,
                    dx=dx_ip6, dpx=dpx_ip6, tag='stage1'),
            xt.TargetSet(at=xt.END,
                    tars=('betx', 'bety', 'alfx', 'alfy', 'dx', 'dpx'),
                    value=boundary_conditions_right),
            xt.TargetRelPhaseAdvance('mux', mux_ir6),
            xt.TargetRelPhaseAdvance('muy', muy_ir6),
        ],
        vary=(
            [xt.VaryList([
                f'kqt13.l6{bn}', f'kqt12.l6{bn}', f'kqtl11.l6{bn}', f'kq10.l6{bn}',
                f'kq9.l6{bn}', f'kq8.l6{bn}', f'kq5.l6{bn}',
                f'kq5.r6{bn}', f'kq8.r6{bn}', f'kq9.r6{bn}',
                f'kq10.r6{bn}', f'kqtl11.r6{bn}', f'kqt12.r6{bn}', f'kqt13.r6{bn}'])]
            + [xt.Vary((f'kq4.r6{bn}' if bn == 'b1' else f'kq4.l6{bn}'))]
        )
    )

    # No staged match for IR6
    if solve:
        opt.solve()

    return opt

def rematch_ir7(collider, line_name,
              boundary_conditions_left, boundary_conditions_right,
              mux_ir7, muy_ir7,
              alfx_ip7, alfy_ip7,
              betx_ip7, bety_ip7,
              dx_ip7, dpx_ip7,
              match_on_aperture=0, epsap=0.0,
              solve=True, staged_match=False, default_tol=None):

    assert line_name in ['lhcb1', 'lhcb2']
    bn = line_name[-2:]

    targets = [
            xt.TargetSet(at='ip7',
                    alfx=alfx_ip7, alfy=alfy_ip7, betx=betx_ip7, bety=bety_ip7,
                    dx=dx_ip7, dpx=dpx_ip7, tag='stage1'),
            xt.TargetSet(at=xt.END,
                    tars=('betx', 'bety', 'alfx', 'alfy', 'dx', 'dpx'),
                    value=boundary_conditions_right),
            xt.TargetRelPhaseAdvance('mux', mux_ir7),
            xt.TargetRelPhaseAdvance('muy', muy_ir7),
        ]
    if match_on_aperture == 1:
        if bn == 'b1':
            lhcb1_targets = [
                xt.TargetSet('bety', xt.LessThan(180.49 - 0.33), at="mq.11l7.b1"),
                xt.TargetSet('bety', xt.LessThan(174.5),at="mq.9l7.b1"),
                xt.TargetSet('bety', xt.LessThan(176.92),  at="mq.8r7.b1"),
                xt.TargetSet('bety', xt.LessThan(179), at="mq.10r7.b1"),
            ]
            targets.extend(lhcb1_targets)
        else:
            lhcb2_targets = [
                xt.TargetSet('bety', xt.LessThan(181.6 + epsap), at="mq.10l7.b2"),
                xt.TargetSet('bety', xt.LessThan(177 + 3.8 + epsap),  at="mq.8l7.b2"),
                xt.TargetSet('bety', xt.LessThan(161.5 + 1.0 + epsap), at="mq.9r7.b2"),
                xt.TargetSet('bety', xt.LessThan(182.5 + 3.1 + epsap), at="mq.11r7.b2"),
                xt.TargetSet('betx', xt.LessThan(179.42 + epsap),  at="mq.12r7.b2"),
                xt.TargetSet('betx', xt.LessThan(185 + epsap),  at="mq.11l5.b2"),
                xt.TargetSet('dx', xt.LessThan(2.30), at="mq.11l5.b2"),
            ]
            targets.extend(lhcb2_targets)

    opt = collider[f'lhc{bn}'].match(
        solve=False,
        default_tol=default_tol,
        start=f's.ds.l7.{bn}', end=f'e.ds.r7.{bn}',
        # Left boundary
        init=boundary_conditions_left, init_at=xt.START,
        targets=targets,
        vary=[
            xt.VaryList([
                f'kqt13.l7{bn}', f'kqt12.l7{bn}', f'kqtl11.l7{bn}',
                f'kqtl10.l7{bn}', f'kqtl9.l7{bn}', f'kqtl8.l7{bn}',
                f'kqtl7.l7{bn}', f'kq6.l7{bn}',
                f'kq6.r7{bn}', f'kqtl7.r7{bn}',
                f'kqtl8.r7{bn}', f'kqtl9.r7{bn}', f'kqtl10.r7{bn}',
                f'kqtl11.r7{bn}', f'kqt12.r7{bn}', f'kqt13.r7{bn}'
            ])
        ]
    )

    if solve:
        if staged_match:
            opt.disable_targets(tag='stage1')
            opt.solve()
            opt.enable_targets(tag='stage1')
            opt.solve()
        else:
            opt.solve()

    return opt

def rematch_ir8(collider, line_name,
                boundary_conditions_left, boundary_conditions_right,
                mux_ir8, muy_ir8,
                alfx_ip8, alfy_ip8,
                betx_ip8, bety_ip8,
                dx_ip8, dpx_ip8,
                solve=True, staged_match=False, default_tol=None):

    assert line_name in ['lhcb1', 'lhcb2']
    bn = line_name[-2:]

    opt = collider[f'lhc{bn}'].match(
        solve=False,
        default_tol=default_tol,
        start=f's.ds.l8.{bn}', end=f'e.ds.r8.{bn}',
        # Left boundary
        init=boundary_conditions_left, init_at=xt.START,
        targets=[
            xt.TargetSet(at='ip8',
                    alfx=alfx_ip8, alfy=alfy_ip8, betx=betx_ip8, bety=bety_ip8,
                    dx=dx_ip8, dpx=dpx_ip8, tag='stage1'),
            xt.TargetSet(at=xt.END,
                    tars=('betx', 'bety', 'alfx', 'alfy', 'dx', 'dpx'),
                    value=boundary_conditions_right, tag='stage2'),
            xt.TargetRelPhaseAdvance('mux', mux_ir8),
            xt.TargetRelPhaseAdvance('muy', muy_ir8),
        ],
        vary=[
            xt.VaryList([
                f'kq6.l8{bn}', f'kq7.l8{bn}',
                f'kq8.l8{bn}', f'kq9.l8{bn}', f'kq10.l8{bn}', f'kqtl11.l8{bn}',
                f'kqt12.l8{bn}', f'kqt13.l8{bn}']
            ),
            xt.VaryList([f'kq4.l8{bn}', f'kq5.l8{bn}'], tag='stage1'),
            xt.VaryList([
                f'kq4.r8{bn}', f'kq5.r8{bn}', f'kq6.r8{bn}', f'kq7.r8{bn}',
                f'kq8.r8{bn}', f'kq9.r8{bn}', f'kq10.r8{bn}', f'kqtl11.r8{bn}',
                f'kqt12.r8{bn}', f'kqt13.r8{bn}'],
                tag='stage2')
        ]
    )

    if solve:
        if staged_match:
            opt.disable_targets(tag=['stage1', 'stage2'])
            opt.disable_vary(tag=['stage1', 'stage2'])
            opt.solve()

            opt.enable_vary(tag='stage1')
            opt.solve()

            opt.enable_targets(tag='stage1')
            opt.enable_targets(tag='stage2')
            opt.enable_vary(tag='stage2')
            opt.solve()
        else:
            opt.solve()

    return opt
